Fs = 2e4
cd '/Users/nima/Local/MEA'
h5disp('17_11_58.h5')
sp = h5read('17_11_58.h5','/proc0/spikeTimes')
ch = h5read('17_11_58.h5','/mapping')

[ch.electrode ch.channel ch.x ch.y]

plot(ch.x,ch.y,'.')

hold on
plot(ch.x(find(ch.x>3000)),ch.y(find(ch.x>3000)),'r.')

badchan = find(ch.electrode==-1)
goodchan = setdiff(1:numel(ch.x),badchan);

U = [sp.frameno-min(sp.frameno)+1 sp.channel+1 repmat(1,numel(sp.frameno),1)];
sU = spconvert(double(U));

tframe = unique(sp.frameno-min(sp.frameno)+1);


for ww = 1:numel(tframe)
	idx = find(sU(tframe(ww),:));
end

figure
xlim([2238 3670])
ylim([880 1800])
for ww=min(tframe):max(tframe)
	title(num2str(ww))
	if ismember(ww,tframe)
		idx = find(sU(tframe(ww),:));
		plot(ch.x(idx), ch.y(idx) ,'*')
		xlim([2238 3670])
		ylim([880 1800])
		drawnow
	end
end

%====== all frames: this video needs fix===========
fig100 = figure
subplot(2,1,1)
plot(ch.x,ch.y,'.')
xlim([2.2095e+03 2.6548e+03])
ylim([1.3598e+03 1800])
axis square;axis off

subplot(2,1,2)
plot(ch.x,ch.y,'.')
xlim([3.2443e+03 3.6905e+03])
ylim([840.3498 1.3465e+03])
axis square;axis off

set(gca,'nextplot','replacechildren'); 


loops = 1e2;
F(loops) = struct('cdata',[],'colormap',[]);

v = VideoWriter('spikePattern.mp4','MPEG-4');
% v.CompressionRatio = 3;

jj=0;
for ww=1:loops%min(tframe):max(tframe)
	jj = jj + 1;
	subplot(2,1,1);
	title(num2str(ww))
	if ismember(ww,tframe)
		subplot(2,1,1);
		hold on
		idx = find(sU(tframe(ww),:));
		h1 = plot(ch.x(idx), ch.y(idx) ,'r*');
		subplot(2,1,2)
		hold on
		idx = find(sU(tframe(ww),:));
		h2 = plot(ch.x(idx), ch.y(idx) ,'r*');
		drawnow
	end
    F(jj) = getframe(fig100);
	delete(h1);delete(h2)
end

ztmp = uint8(240*ones(840,1120,3));
jj=0
for ww=1:loops
	jj=jj+1;
	if ~ismember(ww,tframe)
		F(jj).cdata = ztmp;
	end
end


open(v)
writeVideo(v,F);
close(v);

A=[];for nn=1:1e4;A=[A;size(F(nn).cdata)];end

%====== only frames that have spikes
fig100 = figure
subplot(2,1,1)
plot(ch.x,ch.y,'.')
xlim([2.2095e+03 2.6548e+03])
ylim([1.3598e+03 1800])
axis square;axis off

subplot(2,1,2)
plot(ch.x,ch.y,'.')
xlim([3.2443e+03 3.6905e+03])
ylim([840.3498 1.3465e+03])
axis square;axis off

set(gca,'nextplot','replacechildren'); 


loops = 1e4;
%F(loops) = struct('cdata',[],'colormap',[]);

v = VideoWriter('spikePattern.avi');
% v.CompressionRatio = 3;

jj=0;
for ww=1:loops%min(tframe):max(tframe)
	subplot(2,1,1);
	title(num2str(ww))
	if ismember(ww,tframe)
		jj=jj+1;
		subplot(2,1,1);
		hold on
		idx = find(sU(tframe(ww),:));
		h1 = plot(ch.x(idx), ch.y(idx) ,'r*');
		subplot(2,1,2)
		hold on
		idx = find(sU(tframe(ww),:));
		h2 = plot(ch.x(idx), ch.y(idx) ,'r*');
		drawnow
	    F(jj) = getframe(fig100);
		delete(h1);delete(h2)
	end
end

open(v)
writeVideo(v,F);
close(v);


%=====
sum(sU,1)

sum(sU,2)


%=====

gr1idx = find(ch.x<3000);
gr2idx = find(ch.x>3000);
figure
plot(ch.x(gr1idx),ch.y(gr1idx),'r.')
hold on
plot(ch.x(gr2idx),ch.y(gr2idx),'b.')


v = full(sum(sU,1));
[xq,yq] = meshgrid(1:length(ch.x(gr1idx)),1:length(ch.y(gr1idx)));
vq = griddata(ch.x(gr1idx),ch.y(gr1idx),v(gr1idx),xq,yq);
mesh(xq,yq,vq)
hold on
plot3(ch.x(gr1idx),ch.y(gr1idx),v(gr1idx),'o')

figure
plot3(ch.x,ch.y,v,'o')
hold on
surf(xq,yq,vq)


X = ch.x(gr1idx);
Y = ch.y(gr1idx);
Z = v(gr1idx);
plot3(X,Y,Z)

scatter(X,Y,30,Z,'filled')

scatter3(X,Y,Z,'filled') 

scatter3(X,Y,Z,[],Z,'filled')  % zoom in and out

scatter3(ch.x,ch.y,v,[],v,'filled') 

addpath /Users/nima/Documents/MATLAB/Add-Ons/Collections/matlabAdditionalTool/matcentral/plot3k

plot3k({X Y Z}) 
xlim([2038 2700])
ylim([1200 1900])

plot3k({X Y Z},'plottype','stem') 

plot3k({X Y Z},                                      ...
      'Plottype','stem','FontSize',12,                           ...
      'ColorData',Z,'ColorRange',[-0.5 0.5],'Marker',{'o',2}, ...
      'Labels',{'Peaks','Radius','','Intensity','Lux'});


%========
h5read('17_11_58.h5','/sig',[1 1],[10 1024])

plot(h5read('17_11_58.h5','/sig',[1 1],[100000 1]))

%=========
data = h5read('17_11_58.h5','/sig',[1 100],[114200 1]); %12014200
Fs = 2e4;
[b,a] = butter(2,[300 5000]/(3e4/2));
% Filtering the 1st channel here. Note that raw.Data needs to be typecast to double:
mua = filtfilt(b,a,double(data));
spkwin = -12:20; % -0.6 ms to +1 ms at 20kHz sample rate % -18:30 is equivalent for 30k
threshold = 4.5 * (median(abs(mua)/0.6745));
% note that threshold is not negative here because I'm flipping the signal to detect troughs
[~,locs] = findpeaks(-mua,'minpeakheight',threshold);
locs(locs < 19 | locs > length(mua)-30) = []; % remove detections too early or late
spks = zeros(length(locs),length(spkwin));
for p = 1:length(locs)
    spks(p,:) = mua(locs(p)+spkwin);
end
spkt = locs/Fs;
% plot spikes:
figure();
plot(spkwin/30,spks');
threshold = -4.5 * (median(abs(mua)/0.6745));


%======= spikes per sensor
A=[]
for ww=1:size(sU,2)
	A=[A;numel(find(sU(:,ww)))];
end

plot(A)
print('-djpeg','numberOfSpikesPerSensor')

[aa,bb] =sort(A,'descend');

%======= ISI
FigH=figure('units','normalized','outerposition',[0 0 1 1])
for ww=1:100
	subplot(10,10,ww)
	hist(diff(find(sU(:,bb(ww)))),100);axis off
end
Q = getframe(FigH);
imwrite(Q.cdata, 'top100sensorISI.jpg', 'jpg')

%======== plot avg acti ity grid 2
X = ch.x(gr2idx);
Y = ch.y(gr2idx);
v = full(sum(sU,1));
Z = v(gr2idx);

[xq,yq] = meshgrid(1:length(X),1:length(Y));
vq = griddata(X,Y,Z,xq,yq);

figure
mesh(xq,yq,vq)
hold on
plot3(X,Y,Z,'o')

figure
plot3k({X Y Z},'plottype','stem') 

%=========== 
mzidx = find(Z==max(Z));

figure
plot(X,Y,'.')
hold on
plot(X(mzidx),Y(mzidx),'r*')
print('-djpeg','grid2_mostActiveSite')

%=========== plot grid 2, use dot size as mean activity measure
figure; hold on
for kk=1:numel(Z);
	plot(X(kk),Y(kk),'.','markersize',Z(kk)/max(Z)*20+1)
end

print('-djpeg','grid2_activeSites')

cmap = colormap(jet)
% to do for color
%========== find before and after activation for the most active electrode
dt = [-20 20];

X = ch.x(gr2idx);
Y = ch.y(gr2idx);
v = full(sum(sU,1));
Z = v(gr2idx);
mzidx = find(Z==max(Z));
msidx = find(sU(:,gr2idx(mzidx)));

cmtx=zeros(numel(dt(1):dt(2)),numel(gr2idx));
for kk=1:numel(msidx)
	cmtx = cmtx+full(sU(msidx(kk)+dt(1):msidx(kk)+dt(2),gr2idx));
	%figure; imagesc(full(sU(msidx(kk)-50:msidx(kk)+50,:)))
end

figure
imagesc(cmtx)
colormap hot;colorbar
print('-djpeg','grid2_mostActiveTrigMatrix')

% before
imagesc(cmtx(1:20,:))

%after
imagesc(cmtx(22:end,:))

% zoom
imagesc(cmtx)
ylim([18 24])
colormap hot;colorbar
print('-djpeg','grid2_mostActiveTrigMatrix_zoom')

bZ = mean(cmtx(1:20,:),1);
zZ = mean(cmtx(21,:),1);
aZ = mean(cmtx(22:end,:),1);

figure;plot(bZ,'.')
hold on;plot(zZ,'ro')
hold on;plot(aZ,'g^')


tbZ = bZ-min(bZ);
tbZ = tbZ/max(tbZ);


figure('position',[400 400 800 800]); 
for kk=1:numel(Z);
	subplot(1,2,1);hold on
	plot(X(kk),Y(kk),'.','markersize',bZ(kk)/max(bZ)*20+1);axis square
	% subplot(3,1,2);hold on
	% plot(X(kk),Y(kk),'.','markersize',zZ(kk)/max(zZ)*20+1);% axis off;axis square
	subplot(1,2,2);hold on
	plot(X(kk),Y(kk),'.','markersize',aZ(kk)/max(aZ)*20+1);axis square	
	% drawnow
end
subplot(1,2,1); title('avg activity 20 ms before')
subplot(1,2,2); title('avg activity 20 ms after')
print('-djpeg','grid2_mostActiveSiteBeforeAfter')


%==== finding missing in the grid
ux = unique(X);
uy = unique(Y);

[p,q] = meshgrid(ux,uy);
allpairs = [p(:) q(:)];

figure
plot(p(:),q(:),'g.')
hold on
plot(X,Y,'b.')

norec = setdiff([p(:) q(:)],[X Y],'row')
plot(norec(:,1),norec(:,2),'r*')
print('-djpeg','grid2missingElectrode')

% add the missing elec
NX = [X;norec(:,1)]; 
NY = [Y;norec(:,2)];
NZ = [Z';nan(size(norec,1),1)];

figure;plot3(NX,NY,NZ,'.')

% to fix: meshgrid creates nan data now
% [nxq,nyq] = meshgrid(1:length(NX),1:length(NY));
% nvq = griddata(NX,NY,NZ,nxq,nyq);

%======= find the orders for 2d matrix
% ridx = [];
% for kk=1:size(NX,1)
% 	ridx=[ridx;find(sum(ismember([p(:) q(:)], [NX(kk) NY(kk)]),2)==2)];
% end

% or
ridx = [];
for kk=1:size(NX,1)
	[~,tmp]=ismember([p(:) q(:)],[NX(kk) NY(kk)],'rows');
	ridx(kk,1) = find(tmp);
end


% reorder
[aa,bb] = sort(ridx)
rNX = NX(bb);
rNY = NY(bb);
rNZ = NZ(bb);

figure; hold on
for kk=1:numel(NZ);
	if ~isnan(NZ(kk))
		plot(NX(kk),NY(kk),'.','markersize',NZ(kk)/max(NZ)*20+1)
	end
end

% reshape and show surf
mZ = reshape(rNZ,23,23);

% show surf w missing elec
figure;surf(p,q,mZ)
view([0 90])
colorbar
% print('-djpeg','grid2totalActivitySurface')

% with imagesc
nanidx = find(isnan(mZ(:)));
pcolor(mZ)
print('-djpeg','grid2totalActivitySurface')

%===== skeletonization
BW=mZ;
BW = BW/max(BW(:));
BW(find(isnan(BW(:)))) = rand(size(find(isnan(BW(:)))))
figure
imagesc(flipud(BW))

% BW2 = bwmorph(~isnan(mZ),'skel','inf'); % gets nan skel
% imagesc(flipud(BW2))

%==== gradual cooling of the array 2
X = ch.x(gr2idx);
Y = ch.y(gr2idx);
nsU = sU; % a copy of sparse matrix 
dt = [-1 1]; % for zeroing the frames exactly before and after the spikes of the most active

FigH=figure('units','normalized','outerposition',[0 0 1 1])

for hh = 1:16
	% FIND INDEX OF THE MOST ACTIVE
	nzv = full(sum(nsU,1)); 
	nzZ = nzv(gr2idx);
	mzidx = find(nzZ==max(nzZ));
	msidx = find(nsU(:,gr2idx(mzidx)));

	% plot normalized
	subplot(4,4,hh); hold on
	for kk=1:numel(nzZ);
		plot(X(kk),Y(kk),'.','markersize',nzZ(kk)/max(nzZ)*20+1)
	end
	drawnow;title(['iter#',num2str(hh)])
	rectangle('Position',[X(mzidx)-7 Y(mzidx)-7 14 14],'Curvature',0.2,'LineWidth',1)
	axis square;axis off;axis tight;

	% zero the deltat frames for every spike
	for ww=1:numel(msidx)
		if msidx(ww)+dt(1) ~=0 || msidx(ww)+dt(2)>size(nsU,1)
			nsU(msidx(ww)+dt(1):msidx(ww)+dt(2),gr2idx)	= 0;
		elseif msidx(ww)+dt(1)<1
			ndt = dt(1);
			while msidx(ww)+ndt(1)<1
				ndt = ndt+1;
			end
			nsU(msidx(ww)+ndt:msidx(ww)+dt(2),gr2idx) = 0;			
		elseif msidx(ww)+dt(2)>size(nsU,1)
			ndt = dt(2);
			while msidx(ww)+ndt(2)>size(nsU,1)
				ndt = ndt-1;
			end			
			nsU(msidx(ww)+dt(1):msidx(ww)+ndt,gr2idx) = 0;			
		end
	end
end

Q = getframe(FigH);
imwrite(Q.cdata, 'array2iterativeCooling.jpg', 'jpg')


%====== find the indices for somas for xcorr
X = ch.x(gr2idx);
Y = ch.y(gr2idx);
nsU = sU; % a copy of sparse matrix 
dt = [-1 1]; % for zeroing the frames exactly before and after the spikes of the most active

FigH=figure('units','normalized','outerposition',[0 0 1 1])

somaidx=[];
for hh = 1:16
	% FIND INDEX OF THE MOST ACTIVE
	nzv = full(sum(nsU,1)); 
	nzZ = nzv(gr2idx);
	mzidx = find(nzZ==max(nzZ));
	msidx = find(nsU(:,gr2idx(mzidx)));

	% zero the deltat frames for every spike
	for ww=1:numel(msidx)
		if msidx(ww)+dt(1) ~=0 || msidx(ww)+dt(2)>size(nsU,1)
			nsU(msidx(ww)+dt(1):msidx(ww)+dt(2),gr2idx)	= 0;
		elseif msidx(ww)+dt(1)<1
			ndt = dt(1);
			while msidx(ww)+ndt(1)<1
				ndt = ndt+1;
			end
			nsU(msidx(ww)+ndt:msidx(ww)+dt(2),gr2idx) = 0;			
		elseif msidx(ww)+dt(2)>size(nsU,1)
			ndt = dt(2);
			while msidx(ww)+ndt(2)>size(nsU,1)
				ndt = ndt-1;
			end			
			nsU(msidx(ww)+dt(1):msidx(ww)+ndt,gr2idx) = 0;			
		end
	end
	somaidx=[somaidx;mzidx];
end

gr2somaidx = gr2idx(somaidx);


%=== write for nb
[aa,bb,cc] = find(sU(:,gr2somaidx))
dlmwrite('somaidx.dat',[aa bb],'delimiter','\t','precision','%d')

%=== calculate xcorr
pairidx = combnk(gr2somaidx,2);4

dt = [-300 300]; % for zeroing the frames exactly before and after the spikes of the most active
tvec = dt(1):dt(2);

for kk=1:numel(pairidx)
	xc1=zeros(numel(dt(1):dt(2)),numel(n1idx));
	xc2=xc1;
	xsU = sU(:,pairidx(kk,:));
	n1idx = find(xsU(:,1));
	n2idx = find(xsU(:,2));
	
	% coarse grain to 1 ms
	n1idx = ceil(find(xsU(:,1))/2e2);
	n2idx = ceil(find(xsU(:,2))/2e2);

	% make the ms sparse martrix
	msmtx = spconvert([[n1idx;n2idx],[ones(numel(n1idx),1); 2*ones(numel(n2idx),1)],ones([numel(n1idx)+numel(n2idx)],1)]);	
	
	% calculate cross correlation
	h = waitbar(0,'Please wait...');
	for ww=1:numel(n1idx)	
		% find the value of dt1
		tmp(1) = dt(1);
		while n1idx(ww)+tmp(1)<1 %|| n2idx(ww)+tmp(1)<1
			tmp(1) = tmp(1)+1;
		end;%display(num2str(tmp+1));end
		% find the value of dt2
		tmp(2) = dt(2);
		while n1idx(ww)+tmp(2)>max(union(n1idx,n2idx))% || n2idx(ww)+tmp(2)>max(union(n1idx,n2idx))
			tmp(2) = tmp(2)-1;
		end

		% zero vector
		s1vec = zeros(numel(dt(1):dt(2)),1);
		s2vec = zeros(numel(dt(1):dt(2)),1);

		% find the values form ms sparse matrix
		tmpmtx = full(msmtx(n1idx(ww)+tmp(1):n1idx(ww)+tmp(2),:)); 
		n1vec = tmpmtx(:,1);
		n2vec = tmpmtx(:,2);

		% construct spike vec
		s1vec(find(tvec==tmp(1)):find(tvec==tmp(2))) = svec(find(tvec==tmp(1)):find(tvec==tmp(2))) + n1vec;
		s2vec(find(tvec==tmp(1)):find(tvec==tmp(2))) = svec(find(tvec==tmp(1)):find(tvec==tmp(2))) + n2vec;

		% add spike vec to matrix
		xc1(:,ww) = s1vec;
		xc2(:,ww) = s2vec;
		waitbar(ww/numel(n1idx),h)
	end
	close(h)
	figure;
	imagesc(1:numel(n1idx),tvec,xc1)
	print('-djpeg',['sensor',num2str(pairidx(kk,1)),'_spk_autocorr'])
	figure;
	imagesc(1:numel(n1idx),tvec,xc2)
	print('-djpeg',['sensor',num2str(pairidx(kk,1)),'sensor',num2str(pairidx(kk,2)),'_spk_xcorr'])

	tmpmtx = full(msmtx);
	[acor,lag] = xcorr(tmpmtx(:,1),tmpmtx(:,1),dt(2));
	figure
	plot(lag,acor)
	print('-djpeg',['sensor',num2str(pairidx(kk,1)),'_autocorr'])
	[acor,lag] = xcorr(tmpmtx(:,2),tmpmtx(:,2),dt(2));
	figure
	plot(lag,acor)
	print('-djpeg',['sensor',num2str(pairidx(kk,2)),'_autocorr'])	
	[acor,lag] = xcorr(tmpmtx(:,1),tmpmtx(:,2),dt(2));
	figure
	plot(lag,acor)
	print('-djpeg',['sensor',num2str(pairidx(kk,1)),'sensor',num2str(pairidx(kk,2)),'_xcorr'])

	close all
end

%==== gradient
X = ch.x(gr2idx);
Y = ch.y(gr2idx);
v = full(sum(sU,1));
Z = v(gr2idx);

[xq,yq] = meshgrid(X,Y);
vq = griddata(X,Y,Z,xq,yq);

figure
mesh(xq,yq,vq)

ux = unique(X);
uy = unique(Y);

[p,q] = meshgrid(ux,uy);
allpairs = [p(:) q(:)];

figure
plot(p(:),q(:),'g.')
hold on
plot(X,Y,'b.')

norec = setdiff([p(:) q(:)],[X Y],'row')
plot(norec(:,1),norec(:,2),'r*')

% add the missing elec
NX = [X;norec(:,1)]; 
NY = [Y;norec(:,2)];
NZ = [Z';nan(size(norec,1),1)];

figure;plot3(NX,NY,NZ,'.')

% or
ridx = [];
for kk=1:size(NX,1)
	[~,tmp]=ismember([p(:) q(:)],[NX(kk) NY(kk)],'rows');
	ridx(kk,1) = find(tmp);
end


% reorder
[aa,bb] = sort(ridx)
rNX = NX(bb);
rNY = NY(bb);
rNZ = NZ(bb);

figure; hold on
for kk=1:numel(NZ);
	if ~isnan(NZ(kk))
		plot(NX(kk),NY(kk),'.','markersize',NZ(kk)/max(NZ)*20+1)
	end
end

% to fix: meshgrid creates nan data now
[rnxq,rnyq] = meshgrid(rNX,rNY);
rnvq = griddata(rNX,rNY,rNZ,rnxq,rnyq);

figure
mesh(rnxq,rnyq,rnvq)

[px,py] = gradient(rnvq);
figure
contour(rnxq,rnyq,rnvq)
hold on
quiver(rnxq,rnyq,px,py)
hold off

% reshape and show surf
mZ = reshape(rNZ,23,23);
[px,py] = gradient(mZ);
figure
contour(1:23,1:23,mZ,30)
hold on
quiver(1:23,1:23,px,py)
hold off
colormap hot

axis off
print('-djpeg','grid2_totalGradient')


%==== gradient for gradual cooling of the array 2
X = ch.x(gr2idx);
Y = ch.y(gr2idx);

nsU = sU; % a copy of sparse matrix 
dt = [-1 1]; % for zeroing the frames exactly before and after the spikes of the most active


ux = unique(X);
uy = unique(Y);

[p,q] = meshgrid(ux,uy);
allpairs = [p(:) q(:)];

norec = setdiff([p(:) q(:)],[X Y],'row')


FigH=figure('units','normalized','outerposition',[0 0 1 1])

for hh = 1:16
	% FIND INDEX OF THE MOST ACTIVE
	nzv = full(sum(nsU,1)); 
	nzZ = nzv(gr2idx);
	mzidx = find(nzZ==max(nzZ));
	msidx = find(nsU(:,gr2idx(mzidx)));

	% plot normalized
	subplot(4,4,hh); hold on
	% add the missing elec
	NX = [X;norec(:,1)]; 
	NY = [Y;norec(:,2)];
	NZ = [nzZ';nan(size(norec,1),1)];

	%
	ridx = [];
	for kk=1:size(NX,1)
		[~,tmp]=ismember([p(:) q(:)],[NX(kk) NY(kk)],'rows');
		ridx(kk,1) = find(tmp);
	end

	% reorder
	[aa,bb] = sort(ridx)
	rNX = NX(bb);
	rNY = NY(bb);
	rNZ = NZ(bb);

	% reshape and show surf
	mZ = reshape(rNZ,23,23);
	[px,py] = gradient(mZ);
	contour(1:23,1:23,mZ,30)
	hold on
	quiver(1:23,1:23,px,py)
	% hold off
	colormap hot

	drawnow;title(['iter#',num2str(hh)])
	axis square;axis off;axis tight;

	% zero the deltat frames for every spike
	for ww=1:numel(msidx)
		if msidx(ww)+dt(1) ~=0 || msidx(ww)+dt(2)>size(nsU,1)
			nsU(msidx(ww)+dt(1):msidx(ww)+dt(2),gr2idx)	= 0;
		elseif msidx(ww)+dt(1)<1
			ndt = dt(1);
			while msidx(ww)+ndt(1)<1
				ndt = ndt+1;
			end
			nsU(msidx(ww)+ndt:msidx(ww)+dt(2),gr2idx) = 0;			
		elseif msidx(ww)+dt(2)>size(nsU,1)
			ndt = dt(2);
			while msidx(ww)+ndt(2)>size(nsU,1)
				ndt = ndt-1;
			end			
			nsU(msidx(ww)+dt(1):msidx(ww)+ndt,gr2idx) = 0;			
		end
	end
end

Q = getframe(FigH);
imwrite(Q.cdata, 'array2iterativeCoolingGradient.jpg', 'jpg')
%%
%==== electrode array numbers
elecNum = reshape(0:26399,220,120);
elecNum= elecNum';

[I,J] = ind2sub([220 120],elecNum(:)+1);
%%
figure;plot(I,J,'.')
hold on
for kk=1:numel(ch.electrode)
	curid = find(elecNum(:)==ch.electrode(kk));
	plot(I(curid),J(curid),'r.')
end

xlim([-6 226])
ylim([-6 126])
axis equal
axis off

print('-djpeg','MEA_fullarray_plus_recording')

%======= make composite imagesc from microscopy
lay1= imread('Imaging/FeeMicroscope/Specimen_scan_5x_3D-0002/Specimen_scan_5x_3D-0002_p01(c2).TIF');
lay2= imread('Imaging/FeeMicroscope/Specimen_scan_5x_3D-0002/Specimen_scan_5x_3D-0002_p01(c3).TIF');
lay3= imread('Imaging/FeeMicroscope/Specimen_scan_5x_3D-0002/Specimen_scan_5x_3D-0002_p01(c4).TIF');

imagesc(lay1+lay2)
axis equal
axis off
print('-dtiff','RG_full_array')