%% Load data
cd '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts';
addpath(genpath(pwd));
addpath('/home/reflynn/Documents/MATLAB');
folder = '180406';
filename = sprintf('%s/000.raw.h5', folder);

recording = mxw.fileManager(filename);

sp = h5read(filename, '/proc0/spikeTimes');
ch = h5read(filename, '/mapping');

%% Plot electrode locations
figure;
plot(ch.x,ch.y,'.')

%% Find bad channels
badchan = find(ch.electrode==-1);

%% Extract raw data
first_frame = 1;
data_chunk_size = 300000;
last_frame = first_frame + data_chunk_size - 1;
[rdata, ~, ~] = recording.extractRawData(first_frame, data_chunk_size);

%% Filter data
bdata = zeros(data_chunk_size, numel(ch.channel));

% suppress Invalid MinPeakHeight warning
warning('off', 'signal:findpeaks:largeMinPeakHeight');

w = waitbar(0, '');
for i = 1:numel(ch.channel)
    Fs = 2e4;
    % Can use butter or ellip
    [b,a] = butter(2,[300 7000]/(Fs/2));
    % Filtering one channel here
    % Note that rdata needs to be typecast to double
    mua = filtfilt(b,a,double(rdata(:,i)));
    bdata(:,i) = mua;
    waitbar(i/numel(ch.channel), w, sprintf('Channel %d', i));
end

waitbar(1, w, 'Sorting data');
xyc = sortrows([ch.x ch.y transpose(bdata)], [2 1]);
c = xyc(:, [3:end]);

close(w);

%% Load locations
leader = 17;
sniffer = 24;
lag = 150;
pre = 20;

context = lag+pre+1;

xpos = csvread(sprintf('%s/x_pos.csv', folder));
ypos = csvread(sprintf('%s/y_pos.csv', folder));
leadframe = csvread(sprintf('%s/spiketimes_neuron_%d.csv', folder, leader));

leadpos = [xpos(leader), ypos(leader)];
sniffpos = [xpos(sniffer), ypos(sniffer)];

% % Plot and label all neuron locations
% labels = cellstr(num2str([1:numel(xpos)]'));
% plot(xpos, ypos, 'x');
% textfit(xpos, ypos, labels);

figure();
plot(leadpos(1), leadpos(2), 'rx');
textfit(leadpos(1), leadpos(2), num2str(leader));
hold on;
plot(sniffpos(1), sniffpos(2), 'bx');
textfit(sniffpos(1), sniffpos(2), num2str(sniffer));
drawnow;

lcframe = zeros(numel(leadframe)*(context),2);
for i = 1:numel(leadframe)
    s = leadframe(i);
    lcframe(context*(i-1)+1:context*i,:) = [transpose(s-pre:s+lag) ones(context,1)*s];
end

%% Check sniffer spike times
snifframe = csvread(sprintf('%s/spiketimes_neuron_%d.csv', folder, sniffer));
iframe = intersect(lcframe, snifframe);
sframe = zeros(numel(iframe),1);
for i = 1:numel(sframe)
    sframe(i) = lcframe(find(lcframe(:,1)==iframe(i)),2);
end

cframe = [sframe iframe iframe-sframe];

uframe = unique(sframe);
tframe = zeros(numel(uframe)*context,2);
for i = 1:numel(uframe)
    s = uframe(i);
    tframe(context*(i-1)+1:context*i,:) = [transpose(s-pre:s+lag) ones(context,1)*s];
end

%% Spike context movie
sf = 168900;

tic

w = waitbar(0, 'Initializing');

xbounds = [min(ch.x) max(ch.x)];
ybounds = [min(ch.y) max(ch.y)];
xnum = numel(find(ch.y==min(ch.y)));
ynum = numel(find(ch.x==min(ch.x)));

fig = figure('visible', 'off');

st = find(tframe(:,2) == sf, 1);
len = context;
en = st+len-1;

cd '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts'
v = VideoWriter(sprintf('%s/%s_neuron_%d_spike_%d.avi', folder, folder, leader, tframe(st,2)));
v.Quality = 100;
v.FrameRate = 20;

F = struct('cdata', cell(1,len), 'colormap', cell(1,len));

for i = st:en
    t = tframe(i,1);
    s = tframe(i,2);
    ss = iframe(find(sframe==s));
    ct = transpose(reshape(c(:, t), [xnum, ynum]));
    h = imagesc(xbounds, ybounds, ct);
    hold on;
    set(gca,'Ydir','reverse');
    daspect([1 1 1]);
    title([int2str(t), ', ', sprintf('%0.2f', t/20), ' ms']);
    caxis([-100 100]);
    colorbar;
    colormap(jet);
    % leader circle
    if abs(s-t) <= 10
        lcol = 'w';
    else
        lcol = 'k';
    end
    plot(leadpos(1), leadpos(2), 'Marker', 'o', 'MarkerEdgeColor', lcol, 'MarkerFaceColor', 'none', 'MarkerSize', 10, 'LineWidth', 2);
    % sniffer circle
    if min(abs(ss-t)) <= 10
        scol = 'w';
    else
        scol = 'k';
    end
    plot(sniffpos(1), sniffpos(2), 'Marker', 's', 'MarkerEdgeColor', scol, 'MarkerFaceColor', 'none', 'MarkerSize', 10, 'LineWidth', 2);
    drawnow;
    F(i-st+1) = getframe(gcf);
    waitbar((i-st+1)/len, w, 'Working');
end

close(w);

open(v);
writeVideo(v,F);
close(v);

toc