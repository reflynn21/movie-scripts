
addpath(genpath(pwd))

%% Testing new axonal data

path = '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts/180406/000.raw.h5';
outpath = 

% path = '/Volumes/Hannah_Maxwell_Data/maxoneExperiment_Mar2018/180406/2060_75k_axon_traces_2min/';
% outpath = '/Volumes/Maxwell_bio/results/180406/2060_75k_axon_traces_2min/000/axon_movies/';

%path = '~/MAXONE/180406/2061_50k_axon_traces_2min/000.raw.h5'
%outpath = '~/MAXONE/180406/2061_50k_axon_traces_2min/000/axon_movies/'

recording = mxw.fileManager(path);
spikeCount = mxw.activityMap.computeSpikeCount(recording);
meanAmp = mxw.activityMap.computeMeanAmplitude(recording);
spikeRate = mxw.activityMap.computeSpikeRate(recording);
figure('color','w');subplot(1,2,1)
mxw.plot.activityMap(recording, spikeRate, 'Ylabel', 'Spike Rate (Hz)', 'CaxisLim', [0 max(spikeRate/2)],'Figure',false,'Title','Spike Frequency');
xlabel('\mum');ylabel('\mum');xlim([-200 4100]);ylim([-100 2200])

%% Identifying neurons
activityThreshold = 10;
activityThrValue = prctile(spikeCount, 100 - activityThreshold);
selectedElectrodes = (spikeCount > activityThrValue);
subplot(1,2,2)
mxw.plot.activityMap(recording, selectedElectrodes, 'RevertColorMap', false, 'Interpolate', false, 'CaxisLim', [0 1], 'PointSize', 100,'Title', 'Selected Electrodes','Figure',false);
xlabel('\mum');ylabel('\mum');xlim([-200 4100]);ylim([-100 2200])


fixedElectrodes.electrodes = recording.processedMap.electrode(selectedElectrodes);
fixedElectrodes.xpos = recording.processedMap.xpos(selectedElectrodes);
fixedElectrodes.ypos = recording.processedMap.ypos(selectedElectrodes);

tic
%[axonTraces, electrodeGroups, timestamps] = mxw.axonalTracking.computeAxonTraces(recording, fixedElectrodes, 'SecondsToLoadPerIteration', 10, 'TotalSecondsToLoad', 'full', 'SpikeDetThreshold', 6, 'MaxDistClustering', 100);
[axonTraces, electrodeGroups, timestamps] = mxw.axonalTracking.computeAxonTraces(recording, fixedElectrodes, outpath, 'SecondsToLoadPerIteration', 10, 'TotalSecondsToLoad', 'full', 'SpikeDetThreshold', 6, 'MaxDistClustering', 100);
toc

% %% plot single neurons
% 
% close all;
% for i = [4 13]%1:length(electrodeGroups)
%     
%     figure('color','w','position',[0 0 1500 1500]);
%     subplot(2,2,1);
%     mxw.plot.axonTraces(axonTraces.map.x, axonTraces.map.y, axonTraces.traces{i}, 'PlotFullArea', false, 'PointSize', 150, 'PlotHeatMap', true, 'PlotWaveforms', true,'Figure', false,'Title','Neuron Footprint');
%     xlabel('\mum');ylabel('\mum');axis equal;
%     [val, ind] = min(axonTraces.traces{i},[],1);
%     subplot(2,2,[3 4]);plot(axonTraces.traces{i}(:, ((val<-6))));ylabel('\muV');xlabel('ms')
%     set(gca,'xtick',[0:10:size(axonTraces.traces{i},1)],'xticklabel',[(0:10:size(axonTraces.traces{i},1))/20])
%     box off;  
%     
%     subplot(2,2,2);
%     for j = 1:length(timestamps{i})
%     line([timestamps{i}(j), timestamps{i}(j)],[0.3 0.7],'color','k')    
%     end
%     xlabel('time [s]');title('single neuron time stamps');box off;ylim([0 1])
%     
% end


%% make axon movie

close all;
firstSample = 1;
lastSample = 51;

for p = 1 % add the index of your favorite nneuron

   load('cmap_bluered.mat')
    
    %adjust minimum and maximum values of the colorbar (in uV)
    mini = 5; 
    maxi = 5;
    xx = 0;
    dirName = sprintf(outpath, p);

    mkdir(dirName)
    for j=firstSample:lastSample
        
        xx=xx+1;
        
        clims=[-mini,maxi];
        colormap(mycmap./256)
        plot_2D_map_clean(axonTraces.map.x, axonTraces.map.y, axonTraces.traces{p,1}(j,:), clims, 'nearest');
        
        set(gca,'XTickLabel', '')
        set(gca,'YTickLabel', '')
        hold all
        
        xline = [2800,3800]; % 1 mm scale bar
        yline = [2050,2050];
     
        pl = line (xline,yline,'Color','w','LineWidth',5); % show scale bar
        txt = sprintf('%d ms', round((j-firstSample)/20)); 
        text(300,2050,txt,'Color','w','FontSize',14); %show time
        hold off
        
        pictureName = [sprintf('%03d',xx)];
        
        savepng( 'Directory', dirName , 'FileName' , pictureName );
        
        
    end
    
    close(gcf)
end
  