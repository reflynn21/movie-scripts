%% Load data
cd '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts'
addpath(genpath(pwd))
folder = '180406';
filename = sprintf('%s/000.raw.h5', folder);

recording = mxw.fileManager(filename);

sp = h5read(filename, '/proc0/spikeTimes');
ch = h5read(filename, '/mapping');

%% Plot electrode locations
figure;
plot(ch.x,ch.y,'.')

%% Find bad channels
badchan = find(ch.electrode==-1);

%% Extract raw and BPF data
first_frame = 1;
data_chunk_size = 20000;
last_frame = first_frame + data_chunk_size - 1;
[rdata, ~, ~] = recording.extractRawData(first_frame, data_chunk_size);
[bpfdata, ~, ~] = recording.extractBPFData(first_frame, data_chunk_size);

%% Find spike times
tframe = double.empty(0,1);
U = double.empty(0,3);

bdata = zeros(data_chunk_size, numel(ch.channel));

% suppress Invalid MinPeakHeight warning
warning('off', 'signal:findpeaks:largeMinPeakHeight');

w = waitbar(0, '');
for i = 1:numel(ch.channel)
    Fs = 2e4;
    % Can use butter or ellip
    [b,a] = butter(2,[300 7000]/(Fs/2));
    % Filtering one channel here
    % Note that rdata needs to be typecast to double
    mua = filtfilt(b,a,double(rdata(:,i)));
    bdata(:,i) = mua;

    threshold = 4.5 * (median(abs(mua)/0.6745));
    % note that threshold should not negative here because I'm flipping the signal to detect troughs
    [~,locs] = findpeaks(-mua,'minpeakheight',threshold);
    locs(locs <= 5 | locs > length(mua)-9) = []; % remove detections too early or late
%     spkwin = -5:9; % -0.45 ms to +1 ms at 20kHz sample rate
%     spks = zeros(length(locs),length(spkwin));
%     for p = 1:length(locs)
%         spks(p,:) = mua(locs(p)+spkwin);
%     end
%     spkt = locs/Fs;
%     % plot spikes
%     figure();
%     plot(spkwin/20,spks');
    
    for l = locs
        tframe = [tframe;l-5:l+9];
    end
    splocs = [locs ones(numel(locs),1)*i ones(numel(locs),1)];
    U = [U;splocs];
    waitbar(i/numel(ch.channel), w, sprintf('Channel %d', i));
end

waitbar(1, w, 'Finding unique spike frames');
sframe = unique(U(:,1));

waitbar(1, w, 'Converting to sparse matrix');
U = [U;[last_frame numel(ch.channel) 0]];
sU = spconvert(U);

waitbar(1, w, 'Finding unique context frames');
tframe = unique(tframe);

close(w);

%% Spike context movie
xbounds = [min(ch.x) max(ch.x)];
ybounds = [min(ch.y) max(ch.y)];
xnum = numel(find(ch.y==min(ch.y)));
ynum = numel(find(ch.x==min(ch.x)));

xyc = sortrows([ch.x ch.y transpose(bdata)], [2 1]);
c = xyc(:, [3:end]);

figure('visible', 'off');

cd '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts'
v = VideoWriter(sprintf('%s/%s_spike_context.avi', folder, folder));
v.Quality = 100;
v.FrameRate = 5;
open(v);

for i = 1:156 %length(tframe)
    whitebg('w');
    t = tframe(i);
    ct = transpose(reshape(c(:, t), [xnum, ynum]));
    h = imagesc(xbounds, ybounds, ct);
    hold on;
    daspect([1 1 1]);
    title([int2str(t), ', ', sprintf('%0.2f', t/20), ' ms']);
    caxis([-100 100]);
    colorbar;
    colormap(jet);
    if ~isempty(find(sU(t,:), 1))
        chan = find(sU(t,:));
        plot(ch.x(chan), ch.y(chan), 'Marker', 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'none', 'MarkerSize', 10, 'LineWidth', 2);
    end
    drawnow;
    writeVideo(v, getframe(gcf));
    if i<length(tframe) && tframe(i+1)~=t+1
        clf;
        whitebg('k');
        for j = 1:5
            writeVideo(v, getframe(gcf));
        end
    end
end

close(v);

%% Provided spike times
% tframe = unique(sp.frameno-recording.fileObj.firstFrameNum+1);
% U = [sp.frameno-min(sp.frameno)+1 sp.channel+1 ones(numel(sp.frameno),1)];
% sU = spconvert(double(U));

%% Spike movie
fig100 = figure('visible', 'off');
set(gca,'Ydir','reverse')
whitebg('w');
pbaspect([1 1 1]);
plot(ch.x,ch.y,'.');
xlim([min(ch.x)-35 max(ch.x)+35]);
ylim([min(ch.y)-35 max(ch.y)+35]);
axis off;

set(gca,'nextplot','replacechildren'); 

cd '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts'
v = VideoWriter(sprintf('%s/%s_spikes.avi', folder, folder));
v.Quality = 100;
v.FrameRate = 5;
open(v);

l = 19; %length(sframe);

F = struct('cdata', cell(1,l), 'colormap', cell(1,l));
for jj = 1:l
    ww = sframe(jj);
    title([int2str(ww), ', ', sprintf('%0.2f', ww/20), ' ms']);
	hold on
	chan = find(sU(ww,:));
    h = plot(ch.x(chan), ch.y(chan) ,'r*');
    set(gca,'Ydir','reverse')
	drawnow;
    F(jj) = getframe(fig100);
	delete(h);
    if mod(jj,100) == 0
        disp(jj)
    end
end

open(v);
writeVideo(v,F);
close(v);