Fs = 2e4
h5disp(filename)
sp = h5read(filename,'/proc0/spikeTimes')
ch = h5read(filename,'/mapping')

sampleData = double(h5read(filename,'/sig',[1 2],[1e6,1]));

%% filter design
[b,a] = ellip(2,0.01,60,[300 7000]*2/Fs);
fvtool(b,a,'Fs',Fs)

[bb,aa] = butter(2,[300 7000]*2/Fs);
fvtool(b,a,'Fs',Fs)

%% apply filter
fdat = filtfilt(bb,aa,sampleData);

