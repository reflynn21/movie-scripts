addpath(genpath(pwd))

%% load the data

% 220 x 120 = 26,400

filename = '/home/reflynn/Dropbox (MIT)/MIT/Tegmark_UROP/MEA_data_scripts/001.raw.h5';

recording = mxw.fileManager(filename);
load_position_frame = 1;
data_chunk_size =  20000;

[data, ~, ~] = recording.extractBPFData(load_position_frame, data_chunk_size);
%[data, ~, ~] = recording.extractRawData(load_position_frame, data_chunk_size);

%%
figure()
plot(data)

%% visualize single spike event 
% delay map and a plot of the data (with the 'deepest' channel in black')

% cut out the event
begin_time = 19450
end_time = 19470
map.electrode = recording.rawMap.map.electrode;
map.xpos = recording.rawMap.map.x;
map.ypos = recording.rawMap.map.y;

[amplitudes, times] = min(data(begin_time:end_time,:));  

[val, ind] = min(min(data(begin_time:end_time,:)))
best_electrode = map.electrode(ind); %deepest electrode

xpos_center = map.xpos(ind);
ypos_center =  map.ypos(ind);

nearby_electrodes = [];
nearby_electrodes_indices = [];

figure()
for i = 1:length(map.electrode)
    dist = sqrt((map.xpos(i)-xpos_center)^2 + (map.ypos(i)-ypos_center)^2);
    if dist < 500
         nearby_electrodes = [nearby_electrodes, map.electrode(i)];
         nearby_electrodes_indices = [nearby_electrodes_indices, i]; 
         if ( amplitudes(i) < -50 )
            scatter(map.xpos(i), map.ypos(i), 300, times(i)-times(ind),'filled', 's')
            %colormap hot
            caxis([-5 5])
         end
         hold on;
    end
end
scatter(map.xpos(ind), map.ypos(ind), 300, 'k', 'x')
hold on 

for i = 1:length(nearby_electrodes_indices)
    index = nearby_electrodes_indices(i);
    waveforms = data(begin_time:end_time,index);
    waveformSize = size(waveforms, 1);
    normalizedWaveforms = waveforms ./ val;%./ max(max(abs(waveforms)));
    step = 0.2 * waveformSize / size(normalizedWaveforms, 1);
    left = (map.xpos(index) - 20)';
    waveforms2Plot = (-normalizedWaveforms * 10) + repmat(map.ypos(index), size(normalizedWaveforms,1), 1);
    tempStep = cumsum(repmat(step, size(normalizedWaveforms, 1) + 1, size(normalizedWaveforms, 2)), 1);
    left2Rigth = repmat(left', size(normalizedWaveforms, 1), 1) + tempStep(1:end-1, :);
    plot(left2Rigth, waveforms2Plot, 'color', [0.5 0.2 0.2], 'Linewidth', 2)
    
end



% adding  = 0; 
% for i = 1:length(nearby_electrodes_indices)
%     index = nearby_electrodes_indices(i);
%     adding= adding + 50;
%     data(:,index) = data(:,index) + adding; 
% end


figure()
plot(data(begin_time:end_time,nearby_electrodes_indices))
hold on;
plot(data(begin_time:end_time,ind), 'k')



%% make a map movie of the data 

figure()

xpos = map.xpos(nearby_electrodes_indices);
x = [min(xpos) max(xpos)];

ypos = map.ypos(nearby_electrodes_indices);
y = [min(ypos) max(ypos)];

c = transpose(data(:, nearby_electrodes_indices));

xyc = sortrows([xpos ypos c], [2 1]);

c1 = xyc(:, [3:20002]);

v = VideoWriter(sprintf('%i_%i.avi',begin_time, end_time));
v.Quality = 100;
v.FrameRate = 10;
open(v);

for i = begin_time:end_time
   ci = transpose(reshape(c1(:, i), [20, 20]));
   %h = scatter(map.xpos(nearby_electrodes_indices), map.ypos(nearby_electrodes_indices), 150, transpose(data(i, nearby_electrodes_indices)), 'filled', 's');
   h = imagesc(x, y, ci);
   hold on;
   pbaspect([1 1 1]);
   title([int2str(i), ', ', sprintf('%0.2f', i/20), ' ms'])
   caxis([-200 200])
   colorbar 
   colormap(jet)
   drawnow;
   pause(0.1)
   writeVideo(v, getframe(gcf));
end

close(v);
